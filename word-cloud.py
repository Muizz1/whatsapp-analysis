import matplotlib.pyplot as plt
import pandas as pd
import wordcloud
import sys
import os

# Set workdir and file name
workDir = sys.path[0]
fileName = "WhatsApp-Chat-06.01.2017-08.08.2018.csv"
filePath = os.path.join(workDir, fileName)

# Create dataframe
df = pd.read_csv(filePath)

# Make giant string
text = " ".join([string for string in df['message']])
wordcloud = wordcloud.WordCloud().generate(text)

plt.imshow(wordcloud, interpolation='bilinear')
plt.axis('off')
plt.show()
