# -*- coding: UTF-8 -*-
import pandas as pd
import sys
import re
import os

# Define path
workDir = sys.path[0]
whatsappLogs = 'RAW-WhatsApp-Chat-06.01.2017-08.08.2018.txt'
logsFile = os.path.join(workDir, whatsappLogs)

print(u'\U0001f602'.encode('unicode-escape'))

# Open file
with open(logsFile, 'r', encoding='utf8') as logs:
    text = logs.read().encode('unicode-escape').decode()

# Split at start of text message
textSplit = re.split(r'(\d{2}\/\d{2}\/\d{4}, \d{2}:\d{2})', text)
print(textSplit)
print(len(textSplit))


# Initialize lists for data-fetching
dateList = []
senderList = []
messageList = []

# Start fetching data
for index in range(1, int(len(textSplit) / 2 + 1)):
    date = textSplit[2 * index - 1]
    rawMessage = textSplit[2 * index]
    sender = rawMessage[3]
    message = re.match(r'.+?: (.+)', rawMessage).group(1)
    # Make sure we don't have senders that shouldn't be there.
    assert sender == 'K' or sender == 'M', 'Weird sender at date {}'.format(date)

    # We don't care about media that may be omitted
    if message == "<Media omitted>\n":
        continue

    # Append to lists
    dateList.append(pd.to_datetime(date, dayfirst=True))
    senderList.append(sender)
    messageList.append(message)

print(messageList)
print(len(senderList))

# Make dictionary out of lists in preperation for dataframing
dfDict = {'date' : dateList, 'sender' : senderList, 'message' : messageList}

# Turn dictionary in to dataframe
df = pd.DataFrame(dfDict)

# Write to file.
df.to_csv(os.path.join(workDir, "dataFrame-from-file.csv"))
